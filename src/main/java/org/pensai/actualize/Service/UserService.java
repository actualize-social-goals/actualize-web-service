package org.pensai.actualize.Service;

import org.pensai.actualize.orm.entity.User;
import org.pensai.actualize.orm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        return (List<User>) userRepository.findAll();
    }

    public User createNewUser(User newUser) {
        // Todo add validation
        return userRepository.save(newUser);
    }

    public boolean doesUsernameExist(String username) {
        return userRepository.findByUsername(username) != null;
    }
}
