package org.pensai.actualize.controller;

import org.pensai.actualize.Service.UserService;
import org.pensai.actualize.orm.entity.User;
import org.pensai.actualize.orm.repository.UserRepository;
import org.pensai.actualize.resource.user.UserResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping(path = "/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @PostMapping(path = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<User> saveUser(@RequestBody UserResource userResource) {

        User user = new User();
        user.setUsername(userResource.username);
        user.setPassword(userResource.password);
        user.setEmail(userResource.email);
        user.setTwoFactorAuth(userResource.twoFactorAuth);

        // TODO Add entity validators for business rules once fleshed out

        user = userService.createNewUser(user);

        if (user != null) {
            return ResponseEntity.ok(user);
        }

        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/users")
    public @ResponseBody
    ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getAllUsers();

        return ResponseEntity.ok(users);
    }

    // @GetMapping("/{}")
    // public @ResponseBody ResponseEntity<Boolean> doesUsernameExist(@RequestBody String username) {
    //     if(userService.doesUsernameExist(username)) {
    //         throw new ResponseStatusException(HttpStatus.FOUND, "Username: " + username + " is already taken.");
    //     }
    //     return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    // }
}
