package org.pensai.actualize.security;

import org.springframework.security.crypto.password.PasswordEncoder;

import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import de.mkammerer.argon2.Argon2Factory.Argon2Types;

public class Argon2Encoder implements PasswordEncoder {
    private static final Argon2 argon2 = Argon2Factory.create(Argon2Types.ARGON2id);

    @Override
    public String encode(CharSequence plainTextPassword) {
        return argon2.hash(4, 1024 * 1024, 8, plainTextPassword.toString());
    }

    @Override
    public boolean matches(CharSequence plainTextPassword, String passwordInDatabase) {
        return argon2.verify(passwordInDatabase.toString(), plainTextPassword.toString());
   }
}
