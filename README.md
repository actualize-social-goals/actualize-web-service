# Actualize Web Service
A web service API built with Spring Boot, and OpenAPI. 

# Prerequisites
JDK 15+

Maven

[actulize-orm](https://gitlab.com/Pensai/actualize-orm) installed, configured and entities generated

# Installation
Clone the respository
```
git clone https://gitlab.com/Pensai/actualize-web-service.git
cd actualize-web-service
```
Build with Maven
`mvn clean install -DskipTests`

Run the web service with Maven
`mvn spring-boot:run`

Verify the web service is up and running
```
// Create a user
curl -u admin:admin http://localhost:1970/api/user/create --header "Content-Type: application/json" --request POST --data '{"username": "test", "password":"test123", "email":"test@email.com", "twoFactorAuth":"true"}'

// Fetch a list of users
curl -u admin:admin http://localhost:1970/api/user/users
```
You'll notice that the password is hashed, this is done using argon2 by the web service before saving the users information.

If all is well, move on to setting up the [front end](https://gitlab.com/Pensai/actualize-web-client)
